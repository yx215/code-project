#Code+: Using ML/NLP in Grant Proposal Texts

This notebook is used for testing and practice and making sure that content on Jupyter notebook can be pushed onto Gitlab. 
- jupyter_1 is a file with a program that finds the average length of words in sentences.
- Testing_spacy is a file with a program that finds similarities between sentences
- Song_Similarity is a file with a program that finds the most similar songs based on common words
